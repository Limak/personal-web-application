<?php

$routing = [
    '/' => 'choosePicture',
    '/choosePicture' => 'choosePicture',
    '/pictures' => 'pictures',
    '/pictures_view' => 'pictures',
    '/log_in' => 'log_in_view',
    '/register' => 'register_view',
    '/log_out' => 'logout',
    '/userList' => 'userList',
    '/remember' => 'remember',
    '/remembered' => 'remembered',
    '/forget' => 'forget',
    '/removingPictures' => 'removingPictures'
];