<!DOCTYPE html>

<html lang="pl-PL">
<head>
    <title>Moje Hobby</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <link rel="stylesheet" href="/css/StyleOgolne.css"/>
    <link rel="stylesheet" href="/css/MediaQueries.css"/>
</head>
<body>
<div class="tlo">
    <div class="naglowek">
        <h1 class="czolowka">Podróże</h1>
        <button type="button" id="zmianaKoloru">Zmień kolor</button>
        <?php
        if ($_SESSION['login'] !== null) {
            echo '<span>Zalogowany '.$_SESSION['login'].'</span>';
        }
        ?>
    </div>


    <div class="zawartosc">
        <div class="menu">
            <ul class="pasek_nav">
                <li><a href="/choosePicture">wysyłanie zdjęć</a></li>
                <li><a href="/pictures">przesłane zdjęcia</a></li>
                <li><a href="/remembered">zapamiętane zdjęcia</a></li>
                <?php $userModel = new UserModel();
                if (!$userModel->isLoggedIn()) {
                    echo '<li><a href="/log_in">zaloguj się</a></li>';
                }else {
                    echo '<li><a href="/log_out">Wyloguj się</a></li>';
                }?>
            </ul>
        </div>

        <?php

        if (isset($info)) {
            foreach ($info as $err) {
                echo "<span style=\"color:green;\">$err</span>";
            }
            echo '<br/>';
        }
        if (isset($errors)) {
            echo '<ul>';
            foreach ($errors as $err) {
                echo "<li><span style=\"color:red;\">$err</span></li><br/>";
            }
            echo '</ul><br/>';
        }
        ?>

        <br/><br/>
        <!--tutaj musi byc wczytywanie obrazka-->
        <form method="post" enctype="multipart/form-data">
            <input type="file" name="image" id="image" required/>
            <input type="submit" value="wyślij zdjęcie" name="submit"/>
            <br/>
            <label for="waterMark">Znak Wodny</label>
            <input type="text" name="waterMark" id="waterMark" required/>
            <br/>
            <label for="title">Tytuł</label>
            <input type="text" name="title" id="title" required/>
            <br/>
            <label for="author">Autor</label>
            <input type="text" name="author" id="author" required/>

            <!--dodanie id-->
            <input type="hidden" name="id" value="<?= $picture['_id'] ?>">
        </form>

        <br/>


    </div>

    <div class="stopka">
        <p class="napis_stopka">Autor: Kamil Mastalerz 165296</p>
    </div>

    <script>

        $("#zmianaKoloru").click(function () {
            $(".pasek_nav").css("background-color", "green");
            $(".stopka").css("background-color", "green");
            $("h1.czolowka").css("color", "green");
            $(".dropdown-content").css("background-color", "green");
            $(".dropdown-content:hover").css("color", "green");

            sessionStorage.setItem(".pasek_nav", "green");
            sessionStorage.setItem(".stopka", "green");
            sessionStorage.setItem("h1.czolowka", "green");
            sessionStorage.setItem(".dropdown-content", "green");
            sessionStorage.setItem(".dropdown-content:hover", "green");
        });


        var wczytanyPasek = sessionStorage.getItem(".pasek_nav");
        var wczytanaStopka = sessionStorage.getItem(".stopka");
        var wczytanaCzolowka = sessionStorage.getItem("h1.czolowka");
        var wczytaneRozwijaneMenu = sessionStorage.getItem(".dropdown-content");
        var wczytaneRozwijaneMenuHover = sessionStorage.getItem(".dropdown-content:hover");


        if (wczytanyPasek && wczytanaCzolowka && wczytanaStopka &&
            wczytaneRozwijaneMenu && wczytaneRozwijaneMenuHover) {

            $(".pasek_nav").css("background-color", wczytanyPasek);
            $(".stopka").css("background-color", wczytanaStopka);
            $("h1.czolowka").css("color", wczytanaCzolowka);
            $(".dropdown-content").css("background-color", wczytaneRozwijaneMenu);
            $(".dropdown-content:hover").css("color", wczytaneRozwijaneMenuHover);
        }
        else {
            $(".pasek_nav").css("background-color", "#0174b2");
            $(".stopka").css("background-color", "#0174b2");
            $("h1.czolowka").css("color", "#0174b2");
            $(".dropdown-content").css("background-color", "#0174b2");
            $(".dropdown-content:hover").css("color", "#0174b2");
        }
    </script>
</div>
</body>
</html>