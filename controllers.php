<?php
//require APP . 'business.php';
require APP . 'PhotoModel.php';
require APP . 'UserModel.php';


if (!isset($_SESSION['session-list'])) {
    $_SESSION['session-list'] = array();
}


function choosePicture(&$model)
{
    $photoModel = new PhotoModel();
    $model['errors'] = null;

    $picture = [
        'fileName' => null,
        'thumbNail' => null,
        'author' => null,
        'title' => null
    ];

    //$model['photos'] = $photos;
    if (!isset($_POST["submit"])) {
        return 'choosePicture';
    }

    $targetDir =WEB . 'uploads/images/';

    if (!isset($_FILES['image'])) {
        $model['errors'][] = 'pusty plik';
        return 'choosePicture';
    }
    $originalName = $_FILES["image"]["name"];
    $splitName = explode('.', $originalName);
    $ext = end($splitName);
    $fileName = uniqid("", true) . '.' . $ext;
    $movedDir = $targetDir . $fileName;

    $allowedType = ['image/png',
        'image/jpeg',
        'image/jpg'];

//    $imageType = mime_content_type($_FILES["image"]["tmp_name"]);
    $imageType = $photoModel->getMimeType($_FILES['image']['tmp_name']);

    $readyToUpload = true;

    $fileSize = $_FILES["image"]["size"];

    //wpisany znak wodny
    $waterMark = $_POST['waterMark'];


    $maxSize = 1048576;

    if ($fileSize > $maxSize) {
        $model['errors'][] = 'Za duży plik';
        $readyToUpload = false;
    }

    if (!in_array($imageType, $allowedType)) {
        $model['errors'][] = 'Zly format pliku';
        $readyToUpload = false;
    }

    //koniec wymogow wysylanego zdjecia


    //sprawdzanie czy mozna wyslac a nastepnie wyslanie na serwer

    if ($readyToUpload == true) {
        move_uploaded_file($_FILES["image"]["tmp_name"], $movedDir);


//        add_watermark($fileName, $waterMark);
        $photoModel->add_watermark($fileName,$waterMark);

//        $waterMarkPath = find_with_watermark_path($fileName);
        $waterMarkPath = $photoModel->find_with_watermark_path($fileName);
//        $waterMarkPath = 'uploads/images/thumbnails/with_watermark';
        $thumbPath = $photoModel->find_thumb_path($fileName);


        $picture = [
            'fileName' => $waterMarkPath,
            'thumbNail' => $thumbPath,
            'author' => $_POST['author'],
            'title' => $_POST['title']
        ];

        $model['picture'] = $picture;

        $model['info'][] = 'Plik został pomyślnie wysłany';
        $photoModel->addPicture($picture);
        return 'choosePicture';
    } else {
        $model['errors'][] = 'Obrazek nie został wysłany';
        return 'choosePicture';
    }
}




function pictures(&$model)
{
    $photoModel = new PhotoModel();

    if (!isset($_SESSION['session-list'])) {
        $_SESSION['session-list'] = array();
    }

    //$_SESSION['session-list'] = remember();
    $pictures = $photoModel->get_pictures();
    $model['pictures'] = $pictures;
    $model['remembered'] = $_SESSION['session-list'];

    return 'pictures_view';
}

function remember(&$model)
{
    $photoModel = new PhotoModel();

    $pictures = $photoModel->get_pictures();
    $model['pictures'] = $pictures;

    $toRemember = array();
    if (!empty($_POST) && isset($_POST['picture'])) {
        foreach ($_POST['picture'] as $pictureId) {
            if ($photoModel->photoExists($pictureId)) {
                $toRemember[] = $pictureId;
            }
        }
    }
    $_SESSION['session-list'] = $toRemember;
    $model['remembered'] = $toRemember;
    return 'pictures_view';
}

function forget(&$model)
{
    if (!empty($_POST) && isset($_POST['picture'])) {
        $remembered = array_diff($_SESSION['session-list'], $_POST['picture']);
        $_SESSION['session-list'] = $remembered;
    }
    $model['pictures'] = $_SESSION['session-list'];
    return REDIRECT_PREFIX.'/remembered';
}

function remembered(&$model) {
    $photoModel = new PhotoModel();

    $ids = $_SESSION['session-list'];
//    $pictures = $photoModel->getRememberedPictures($ids);
    $pictures = $photoModel->getRememberedPictures($ids);
    $model['pictures'] = $pictures;
    return 'remembered';
}


function log_in_view(&$model)
{
    $userModel = new UserModel();
    if ($userModel->isLoggedIn()) {
        return 'redirect:/';
    }
    if (empty($_POST)) {
        return 'log_in_view';
    }

    if (empty($_POST['password']) ||
        empty($_POST['login'])
    ) {
        $model['errors'][] = 'puste pola';
        return 'log_in_view';
    }


    $login = $_POST['login'];
    $password = $_POST['password'];
    if ($userModel->login($login, $password)) {
        $_SESSION['login'] = $login;
        return 'redirect:/';
    } else {
        $_SESSION['login'] = null;
        $model['errors'][] = 'niepoprawne dane';
    }
    return 'log_in_view';
}


function logout(&$model){
    $userModel = new UserModel();
    $userModel->logout();
    return 'redirect:/log_in';
}

function userList(&$model) {
    return 'userList';
}

function removingPictures(&$model){
    return 'removingPictures';
}

function register_view(&$model)
{
    if (empty($_POST)) {
        return 'register_view';
    }

    if (empty($_POST['login']) ||
        empty($_POST['confirmedPassword']) ||
        empty($_POST['password']) ||
        empty($_POST['email'])
    ) {

        $model['errors'][] = 'Uzupełnij wszystkie pola';
        return 'register_view';
    }
    //require 'Model/userModel.php';
    $_POST['login'] = htmlspecialchars($_POST['login']);

    $db = get_db();

    if ($_POST['password'] !== $_POST['confirmedPassword']) {
        $model['errors'][] = 'hasla są różne';
        return 'register_view';
    }
    if (strlen($_POST['login']) < 4) {
        $model['errors'][] =  'Wybrany login jest zbyt krótki';
        return 'register_view';
    }
    if (!preg_match('/^[A-Za-z][A-Za-z0-9]{4,31}$/', $_POST['login'])) {
        $model['errors'][] =  'Login może składać się tylko z liter,cyfr';
        return 'register_view';
    }
    if (!preg_match('/^[A-Za-z][A-Za-z0-9]{4,31}$/', $_POST['password'])) {
        $model['errors'][] =  'hasło może składać się tylko z liter,cyfr';
        return 'register_view';
    }
    if (strlen($_POST['password']) < 4) {
        $model['errors'][] =  'Wybrane hasło jest za krótkie';
        return 'register_view';
    }
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $model['errors'][] = 'Podany adres email jest nieprawidłowy';
        return 'register_view';
    }

    $authentication = [
        'login' => $_POST['login'],
        'password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
        'email' => $_POST['email']
    ];


    $userModel = new UserModel();

    $userFound = $db->users->findOne(['login' => $authentication['login']]) != null;
    $emailFound = $userModel->userWithEmailExists($authentication['email']);

    if ($userFound || $emailFound) {
        $model['errors'][] = 'dany użytkownik istnieje';
        return 'register_view';
    } else {
        $db->users->insert($authentication);
        $model['info'][] = 'zarejestrowano';
        return 'register_view';
    }
    /** @noinspection PhpUnreachableStatementInspection */
    return 'register_view';
}

