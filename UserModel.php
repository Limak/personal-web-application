<?php
class UserModel
{
    public function userWithEmailExists($email)
    {
        $db = get_db();
        return $db->users->findOne(['email' => $email]) !== null;
    }

    /**
     * @return bool pro dokumentacja
     */
    public function isLoggedIn()
    {
        return isset($_SESSION['logged']) && $_SESSION['logged'] === true;
    }

    public function showAll()
    {
        $users = [];
        $data = get_db()->users->find();
        foreach ($data as $usr) {

            $users[] = [
                'login' => $usr['login'],
                'email' => $usr['email']
            ];
        }
        return $users;
    }

    public function removeAll()
    {
        $users = [];
        $data = get_db()->users->remove();
        foreach ($data as $usr) {
            $users[] = [
                'login' => $usr['login'],
                'email' => $usr['email']
            ];
        }
        return $users;
    }

    public function login($login, $password)
    {
        $db = get_db();
        // findOne zwraca nulla jak nie znajdzie
        /** @var MongoCursor $user */
        $user = $db->users->findOne(['login' => $login]);
        $hash = $user['password'];

        $_SESSION['logged'] = password_verify($password, $hash);
        return $_SESSION['logged'];
    }

    public function logout()
    {
        $_SESSION['logged'] = false;
        session_destroy();
    }
}