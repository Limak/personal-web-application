<?php
define('WEB', dirname(__FILE__) . '/');
define('APP', dirname(dirname(__FILE__)) . '/');

require APP . 'routing.php';
require APP . 'dispatcher.php';
require APP . 'controllers.php';

//aspekty globalne
session_start();
if(!isset($_SESSION['logged'])){
    $_SESSION['logged'] = false;
}

//wybór kontrolera do wywołania:
$action_url = $_GET['action'];


if ($action_url == null) {
    $action_url = '/';
}


/** @noinspection PhpUndefinedVariableInspection */
dispatch($routing, $action_url);

