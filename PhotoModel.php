<?php
require APP . 'business.php';

class PhotoModel
{

    public function get_pictures()
    {
        $db = get_db();
        return $db->pictures->find();
    }

    public function get_picture($id)
    {
        $db = get_db();
        return $db->pictures->findOne(['_id' => new MongoId($id)]);
    }


    public function addPicture(array $picture) {
        $db = get_db();

        $db->pictures->insert($picture);
    }


     public function getMimeType($fileName)
    {
        if (!function_exists('finfo_open')) {
//            return minimime($fileName);
            return $this->minimime($fileName);
        }
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        return finfo_file($finfo, $fileName);
    }

    private function minimime($fname)
    {
        $fh = fopen($fname, 'rb');
        if ($fh) {
            $bytes6 = fread($fh, 6);
            fclose($fh);
            if ($bytes6 === false) return false;
            if (substr($bytes6, 0, 3) == "\xff\xd8\xff") return 'image/jpeg';
            if ($bytes6 == "\x89PNG\x0d\x0a") return 'image/png';
            if ($bytes6 == "GIF87a" || $bytes6 == "GIF89a") return 'image/gif';
            return 'application/octet-stream';
        }
        return false;
    }


    public function make_thumb($fileName)
    {

        //sciezka do pliku gdzie bedzie miniatura ze znakime wodnym
        $destDir =WEB . 'uploads/images/thumbnails/min_' . $fileName;
        //sciezka do pliku ze znakiem wodnym aby go przerobic
        $srcDir =WEB . 'uploads/images/thumbnails/with_watermarks/wm_' . $fileName;
        $fileExtension = pathinfo($srcDir, PATHINFO_EXTENSION);

        if ($fileExtension === "jpg" || $fileExtension === "jpeg") {
            $sourceImage = imagecreatefromjpeg($srcDir);

            $originalWidth = imagesx($sourceImage);
            $originalHeight = imagesy($sourceImage);

            if ($originalHeight > $originalWidth) {
                $desiredHeight = 200;
                $desiredWidth = 125;
            } else {
                $desiredHeight = 125;
                $desiredWidth = 200;
            }


            $virtualIMG = imagecreatetruecolor($desiredWidth, $desiredHeight);

            imagecopyresampled($virtualIMG, $sourceImage, 0, 0, 0, 0,
                $desiredWidth, $desiredHeight, $originalWidth, $originalHeight);
            imagejpeg($virtualIMG, $destDir);
            imagedestroy($virtualIMG);
        } elseif ($fileExtension === "png") {
            $sourceImage = imagecreatefrompng($srcDir);

            $originalWidth = imagesx($sourceImage);
            $originalHeight = imagesy($sourceImage);

            if ($originalHeight > $originalWidth) {
                $desiredHeight = 200;
                $desiredWidth = 125;
            } else {
                $desiredHeight = 125;
                $desiredWidth = 200;
            }

            $virtualIMG = imagecreatetruecolor($desiredWidth, $desiredHeight);

            imagecopyresampled($virtualIMG, $sourceImage, 0, 0, 0, 0,
                $desiredWidth, $desiredHeight, $originalWidth, $originalHeight);

            imagepng($virtualIMG, $destDir);
            imagedestroy($virtualIMG);
        }

    }

    public function find_thumb_path($fileName)
    {
        $destDir = 'uploads/images/thumbnails/min_' . $fileName;
        return $destDir;
    }

    public function find_with_watermark_path($fileName)
    {
        $destDir = 'uploads/images/thumbnails/with_watermarks/wm_' . $fileName;
        return $destDir;
    }

    public function add_watermark($fileName, $waterMarkText)
    {
        $srcDir = 'uploads/images/' . $fileName;
        $destDir = 'uploads/images/thumbnails/with_watermarks/wm_' . $fileName;
        $fileExtension = pathinfo($srcDir, PATHINFO_EXTENSION);

        list($width, $height) = getimagesize($srcDir);

        $font = WEB . 'arial.ttf';

        $fontSize = "50";


        if ($fileExtension === "jpg" || $fileExtension == "jpeg") {
            $sourceIMG = imagecreatefromjpeg($srcDir);
            $destIMG = imagecreatetruecolor($width, $height);

            imagecopyresampled($destIMG, $sourceIMG, 0, 0, 0, 0, $width, $height, $width, $height);
            $watermarkColor = imagecolorallocate($destIMG, 191, 191, 191);

            imagettftext($destIMG, $fontSize, 0, 40, $height - 60, $watermarkColor, $font, $waterMarkText);
            imagejpeg($destIMG, $destDir);

//            make_thumb($fileName);
            $this->make_thumb($fileName);

            //zwalnianie pamieci
            imagedestroy($sourceIMG);
            imagedestroy($destIMG);
        } elseif ($fileExtension === "png") {
            $sourcePNG = imagecreatefrompng($srcDir);
            $destPNG = imagecreatetruecolor($width, $height);

            imagecopyresampled($destPNG, $sourcePNG, 0, 0, 0, 0, $width, $height, $width, $height);
            $watermarkColor = imagecolorallocate($destPNG, 191, 191, 191);

            imagettftext($destPNG, $fontSize, 0, 40, $height - 60, $watermarkColor, $font, $waterMarkText);
            imagepng($destPNG, $destDir);

//            make_thumb($fileName);
            $this->make_thumb($fileName);

            //zwalnianie pamieci
            imagedestroy($sourcePNG);
            imagedestroy($destPNG);
        }

        return $destDir;
    }


    public function get_pictures_from_dir($dir)
    {
        $files = array();
        $handle = opendir($dir);

        if ($handle) {
            $file = readdir($handle);

            while ($file !== false) {
                $files[] = $file;
            }
            closedir($handle);
        }
        return $files;
    }

    //--------------------------------ULUBIONE--------------------------------


    public function photoExists($id) {
        if (empty($id)) return false;
        $db = get_db();
        return $db->pictures->findOne(['_id' => $id instanceof MongoId ? $id : new MongoId($id)]) ? true : false;
//        return $db->pictures->findOne(['_id' => $id]) ? true : false;
    }


    public function getRememberedPictures($ids)
    {
        $db = get_db();
        $mongoIds = array();
        foreach ($ids as $id) {
            $mongoIds[] = $id instanceof MongoId ? $id : new MongoId($id);
        }
        $data = $db->pictures->find(['_id' => ['$in' => $mongoIds]]);
        return $data;
    }


    public function removeAll()
    {
        $images = $this->get_pictures();

        foreach ($images as $image) {
            get_db()->pictures->remove($image);
        }
    }
}